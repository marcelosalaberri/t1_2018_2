#include "nqueens.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <pthread.h>

#define LINHA_VAZIA 100
#define NUM_THREADS 4


//Testa se rainhas nas posições (l1, c1) e (l2, c2) se atacam  -  Obs: l1 é sempre diferente de l2
bool testa_ataque(int l1, int c1, int l2, int c2){
	//linha não possui rainha
	if (c1 == LINHA_VAZIA)
		return true;

	//testa de as rainhas estão na mesma coluna
	if (c1 == c2)
		return false;

	//testa se as rainhas estão em diagonal
	if (abs(c1-c2) == abs(l1-l2))
		return false;

	return true;
}

int proxima_pos(int *tabuleiro, int dim, int linha, int coluna){

	int c, l;

	for (c = coluna; c < dim; c++){
		for (l = 0; l < linha; l++)
			if (!testa_ataque(l, tabuleiro[l], linha, c)) break;
		if (l == linha)
			return c;
	}

	return LINHA_VAZIA;
}

unsigned long long valor_solucao(int *tabuleiro, int dim){
	unsigned long long solucao = 0;

	for (int l = 0; l < dim; l++){
		if (tabuleiro[l] != LINHA_VAZIA){
			solucao += pow(2, l*dim + tabuleiro[l]);
		}
	}

	return solucao;
}


unsigned long long func(int *tabuleiro, int dim, int queens, int linha){
	unsigned long long soma = 0;

	//solucao encontrada
	if (queens == 0 && linha <= dim)
		return valor_solucao(tabuleiro, dim);

	//este caminho não encontrou solução
	if (linha == dim)
		return 0;

		
	int c = proxima_pos(tabuleiro, dim, linha, 0);

	while (c < dim){
		tabuleiro[linha] = c;
		soma += func(tabuleiro, dim, queens-1, linha+1);

		c = proxima_pos(tabuleiro, dim, linha, c+1);
	}

	tabuleiro[linha] = LINHA_VAZIA;
	soma += func(tabuleiro, dim, queens, linha+1);

	return soma;
}


pthread_mutex_t mutex;

typedef struct 
{
	int **tabuleiros; 			//array com os possíveis tabuleiros inicias
	int dim;					//Dim
	int queens;					//Queens
	int i;						//indice do proximo tabuleiro disponível para uso
	unsigned long long soma_total;	//resultado com a soma das soluções
}PAR;


void* thread_func(void *ptr){
	PAR *par = (PAR*) ptr;

	unsigned long long soma_parcial = 0;
	int indice;


	//região critica - pega o indice para proximo tabuleiro inicial e incrementa o indice
	pthread_mutex_lock (&mutex);
	indice = par->i;
	par->i++;
	pthread_mutex_unlock (&mutex);


	//Enquanto ainda tiver tabuleiros iniciais
	while (indice < (par->dim + 1)){
		//O -1 é porque já foi colocado uma rainha na primeira linha
		int queens = par->queens - 1;
		
		//Se for o tabuleiro sem rainha na primeira linha então precisa adicionar 1 de volta
		if (indice == par->dim)
			queens++;

		soma_parcial += func(par->tabuleiros[indice], par->dim, queens, 1);


		//região critica - pega o indice para proximo tabuleiro inicial e incrementa o indice
		pthread_mutex_lock (&mutex);
		indice = par->i;
		par->i++;
		pthread_mutex_unlock (&mutex);
	}


	//região critica - atualiza o valor de soma total com a soma parcial gerada pela thread
	pthread_mutex_lock (&mutex);
	par->soma_total = par->soma_total + soma_parcial;
	pthread_mutex_unlock (&mutex);


	return NULL;
}

unsigned long long nqueens(int dim, int queens){

	//Casos que não deve executar
	if (dim <= 0 || queens <= 0 ||  queens > dim)
		return 0;

	//Casos com uma rainha
	if (queens == 1)
		return pow(2, dim*dim)-1;


	PAR par;

	//Aloca o espaço dos tabuleiros iniciais
	par.tabuleiros = malloc(sizeof(int*) * (dim+1));
	//Coloca os outros parametros na estrutura
	par.i = par.soma_total = 0;
	par.dim = dim;
	par.queens = queens;


	//Aloca cada tabuleiro e preenche eles com a informação de LINHA_VAZIA colocando uma rainha na primeira linha (colunas diferentes para cada tabuleiro).
	for (int i = 0; i < dim + 1; i++){
		par.tabuleiros[i] = malloc(sizeof(int) * dim);

		for (int l = 0; l < dim; l++)
			par.tabuleiros[i][l] = LINHA_VAZIA;

		//Se não for o caso de deixar sem rainha
		if (i < dim)
			par.tabuleiros[i][0] = i;
	}


	pthread_mutex_init(&mutex, NULL);


	pthread_t threads[NUM_THREADS];
	
	//Cria
	for (int i = 0; i < NUM_THREADS; i++){
		pthread_create(&threads[i], NULL, thread_func, (void*) &par);
	}

	//Join
	for (int i = 0; i < NUM_THREADS; i++){
		pthread_join(threads[i], NULL);
	}

	
	//Desaloca os tabuleiros iniciais
	for (int i = 0; i < dim + 1; i++)
		free(par.tabuleiros[i]);
	free(par.tabuleiros);


 	pthread_mutex_destroy(&mutex);
    
	return par.soma_total;
}